#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	List list;
	for (int i = 0; i <= 10; i++) {
		list.headPush(i); //0 1 2 3 4 5 6 7 8 9 10
	}
	list.headPop(); //1 2 3 4 5 6 7 8 9 10
	list.tailPop(); //1 2 3 4 5 6 7 8 9
	list.tailPush(11); //1 2 3 4 5 6 7 8 9 11
	list.deleteNode(2); //1 3 4 5 6 7 8 9 10
	system("pause");
}