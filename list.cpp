#include <iostream>
#include <windows.h>
#include "list.h"

using namespace std;

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::headPush(int data) {

	Node *tmp;

	if (tail == 0 && head == 0) { //check if there are no nodes >>> the push first node
		head = new Node(data, 0, 0); //create new node at haed
		tail = head;
	}
	else { //already have some nodes
		tmp = new Node(data, head, 0);
		head->prev = tmp;
		head = tmp;
	}
}

void List::tailPush(int data) {

	Node *tmp;

	if (tail == 0 && head == 0) { //check if there are no nodes >>> the push first node
		tail = new Node(data, 0, tail); //create new node at tail
		head = tail;
	}
	else { //already have some nodes
		tmp = new Node(data, 0, tail);
		tail->next = tmp; //points to temp
		tail = tmp;
	}

}



int List::headPop() {

	while (!isEmpty()) { //list must not be empty
		Node *tmp = head->next;
		delete head;
		head = tmp;
		head->prev = 0;
		return head->info;
		break; //prevent a loop
		
	}
	cout << "Empty list.";
	return false;
}

int List::tailPop() {

	while (!isEmpty()) { //list must not be empty
		Node *tmp;
		tmp = tail->prev;
		delete tail;
		tail = tmp;
		tmp->next = 0;
		break; //prevent a loop
	}
	cout << "Empty list.";
	return false;
}

void List::deleteNode(int search) {

	if (isEmpty()) { //list must not be empty
		cout << "Empty list.";
		return;
	}
	Node *tmp = head; 
	while (true) {

		if (tmp->info == search) {
			if (tmp == tail) { //if it is the last node
				List::tailPop();
				return;
			}
			else if (tmp == head) { //if it is the first node
				List::headPop(); 
				return;
			}
			else { //not above
				Node *hold = tmp->prev;
				hold->next = tmp->next;
				tmp->next->prev = hold;
				delete tmp;
				tmp = hold;
				return;
			}
		}
		tmp = tmp->next;
		if (tmp == 0) {
			return;
		}
	}
}


bool List::isInList(int search) {

	if (isEmpty()) { //list must not be empty
		cout << "Empty list.";
		return false;
	}

	Node *tmp = tail;
	bool result;
	result = false;
	cout << "The " << search << " are in ";

	while (true) {
		cout << " " << tmp->info << " ";
		if (tmp->info == search) { //founded!
			result = true;
		}
		tmp = tmp->prev; //points next
		if (tmp == 0) {
			break;
		}
	}
	return result;

}
